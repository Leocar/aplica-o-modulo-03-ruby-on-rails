# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



puts "Criando as moeda ........"

mining_types = [
   {description:"Proof of Work", acronym: "PoW"},
   {description:"Proof of Stake", acronym: "PoS"},
   {description:"Proof of Capacity", acronym: "PoC"}
 ]
mining_types.each do |mining_type|
   MiningType.find_or_create_by!(mining_type)
end


Coin.create!(
   description: "Bitcoin",
   acronym: "BTC",
   url_image:"https://usethebitcoin.com/wp-content/uploads/2018/07/bitcoin_PNG47.png",
   mining_type: MiningType.all.sample
)
Coin.create!(
    description: "Ethereum",
    acronym: "ETH",
    url_image:"https://www.google.com/url?sa=i&url=https%3A%2F%2Fdlpng.com%2Fpng%2F5511462&psig=AOvVaw28OoH2sEIhbl3tiU5uxG-G&ust=1584623900719000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCIDSm4eOpOgCFQAAAAAdAAAAABAJ",
    mining_type: MiningType.all.sample
 )
 Coin.create!(
    description: "Dash",
    acronym: "DASH",
    url_image:"https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.dash.org%2Fpt-br%2Fdiretrizes-da-marca%2F&psig=AOvVaw0nSF3ATPOM-0jc6KhsvqAz&ust=1584624384698000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNjxte2PpOgCFQAAAAAdAAAAABAD",
    mining_type: MiningType.all.sample
 )

 
 puts "Moedas Criadas com Sucesso!!!!!"
